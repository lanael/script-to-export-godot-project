# script-to-export-godot-project


## Description
This goal's project is to make a bash script to export godot project to Web (HTML5) without using godot interface.

## Usage
All you need to do is to run the script at the root of your godot project.

## You need to know
If you are using external resources in your godot project, you need to know that they aren't usable on the web if you are loading them with the FileSystem.
The web version of your project is unable to access FileSystem and can't load file that you are storing on your computer.

## Roadmap
Ideas to upgrade the script.

## Project status
For the moment, the script is working but there is a lot that can be upgraded.